var http = require('http').createServer(handler); //require http server, and create server with function handler()
var fs = require('fs'); //require filesystem module
var io = require('socket.io')(http) //require socket.io module and pass the http object (server)
var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var MOTOR = new Gpio(27, 'out'); //use GPIO pin 4 as output  
var MOTOR4 = new Gpio(13, 'out');
var MOTOR5 = new Gpio(19, 'out');
var MOTOR6 = new Gpio(26, 'out');


http.listen(8080); //listen to port 8080

function handler (req, res) { //create server
  fs.readFile(__dirname + '/public/motor.html', function(err, data) { //read file index.html in public folder
    if (err) {
      res.writeHead(404, {'Content-Type': 'text/html'}); //display 404 on error
      return res.end("404 Not Found");
    } 
    res.writeHead(200, {'Content-Type': 'text/html'}); //write HTML
    res.write(data); //write data from index.html
    return res.end();
  });
}

io.sockets.on('connection', function (socket) {// WebSocket Connection
  var lightvalue = 0; //static variable for current status
  socket.on('light', function(data) { //get light switch status from client
    lightvalue = data;
    if (lightvalue != MOTOR.readSync()) { //only change MOTOR if status has changed
      MOTOR.writeSync(lightvalue); //turn MOTOR on or off
      MOTOR4.writeSync(lightvalue);
      MOTOR5.writeSync(lightvalue);
      MOTOR6.writeSync(lightvalue); 
    }
  });
});

process.on('SIGINT', function () { //on ctrl+c
  MOTOR.writeSync(0); // Turn MOTOR off
  MOTOR4.writeSync(0);
  MOTOR5.writeSync(0);
  MOTOR6.writeSync(0);
  MOTOR.unexport(); // Unexport MOTOR GPIO to free resources
  MOTOR4.unexport();
  MOTOR5.unexport();
  MOTOR6.unexport();

  process.exit(); //exit completely
});

