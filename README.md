<h1>保齡球機</h1>
<h5>會用到的技術有websocket & 控制Raspberry Pi 的 ＧPIO</h5>
<br>
<h2>影片連結：</h2>
<h4>https://drive.google.com/open?id=10stLyZ1uIi8dmVY-l1EJcwsLsYfLRjpe</h4>
<br>
<h2>第一步</h2>
<h5>
	首先先建置所需要的環境 node.js , 在Raspberry Pi的終端機輸入下面指令
	<br><br>
	<h4>更新Raspberry Pi的環境</h4>
	<br>
	$ sudo apt-get update
	<br>
	$ sudo apt-get dist-upgrade
	<br>
	<h4>下載Node.js</h4>
	<br>
	$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
	<br>
	<h4>安裝Node.js</h4>
	<br>
	$ sudo apt-get install -y nodejs
	<br>
	<h4>檢查Node.js是否安裝成功</h4>
	<br>
	$ node -v
	<br>
	<h4>安裝控制GPIO所需要的Module</h4>
	<br>
	$ npm install onoff
</h5>
<h2>第二步</h2>
<h5>
	<h4>先在家目錄下建一個資料夾 nodetest</h4>
	<br>
	$ mkdir nodetest
	<br>
	<h4>CD 移至 nodetest資料夾裡面</h4>
	<br>
	$ cd nodetest
	<br>
	<h4>撰寫一個透過網頁控制馬達的js檔</h4>
	<br>
	$ nano webserver.js
	<br>
	<h4>程式碼參照Gitlab上面的webserver.js檔</h4>
</h5>
<h2>第三步</h2>
<h5>
	<h4>在資料夾 nodetest 裡面再建一個資料夾</h4>
	<br>
	$ mkdir public
	<br>
	<h4>CD 移至 public資料夾裡面</h4>
	<br>
	$ cd public
	<br>
	<h4>撰寫一個html檔，可以透過這個介面開關馬達</h4>
	<br>
	$ nano motor.html
	<br>
	<h4>程式碼參照Gitlab上面的motor.html檔/<h4>
</h5>
<h2>第四步</h2>
<h5>
	<h4>CD 回到前一個資料夾 nodetest 裡面</h4>
	<br>
	$ cd ..
	<br>
	<h4>開啟webserver</h4>
	<br>
	$ node webserver.js
	<br>
	<h4>開啟後server後，開啟瀏覽器，在網址的地方打開localhost:8080，便可以在網頁的地方控制馬達</h4>
</h5>

